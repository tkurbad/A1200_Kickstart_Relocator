## Amiga 1200 Kickstart Relocator for Towerized Systems

These are the [KiCad](http://kicad-pcb.org/) sources of a relocator board
that has been designed to move the Amiga 1200's Kickstart ROMs a fair bit
to the front of the mainboard.

### What's it Made for?

First of all, the intention was to get the [FastATA 1200 IDE controller](http://elbox.com/products/fast_ata_1200.html)
out of the way of conflicting expansions like the [Indivision AGA MK2cr](https://icomp.de/shop-icomp/en/produkt-details/product/Flickerfixer_Scandoubler.html#filter=*)
in a **towerized** setup of one of John 'Chucky' Hertells fabulous
[ReAmiga R1200 boards](http://wordpress.hertell.nu/?p=587#comment-52510)
and a [Mediator PCI 1200 TX](http://elbox.com/products/mediator_pci_1200_tx.html),
amongst other things. (**Before you start cheering about relocating the
FastATA see note below!**)

The second goal was to expose the Kickstart sockets from under the
Mediator to make experimenting with different Kickstart ROMs easier.

The third and fourth goal were to expose pin headers
* to enable or disable Kickstart switch functionality with 1 MB (EP)ROMs
(e.g. MC27C800) and to connect an actual switch or jumper to switch
Kickstarts easily
* to break out the two pins needed for [Matthias Münch's Clockport expanders](https://www.amigaworld.de/hardware/)


**The first goal, however, could not be achieved!**
The FastATA is a very sensitive device
when it comes to signal integrity. The additional impedance exerted by
the trace length of the relocator is enough to render the FastATA
useless. While the Amiga still boots fine from its primary port, the
driver doesn't detect the FastATA properly in this configuration.

For me, it did work on approximately 1 out of 10 boots, but it was still
very unstable. *And this was with the relocator directly soldered to the
ReAmiga board and the FastATA soldered directly to the relocator!*

There *might* be four ways to overcome this (or a combination of some of these):
* Shorten the traces somehow. I don't know how this could be done, but
perhaps some PCB routing genius does.
* Make wider traces, which is a bit hard given the limited space to
circumvent the CPU.
* Make the board four layers with GND and VCC layers on the inside for
better signal integrity. This would, however, add significantly to the
cost of having the board produced.
* Buffer the 18 address and 32 output lines. The buffering chips could
be placed on top of the original Kickstart sockets as well as "inside"
the relocated sockets. However, this would demand for some serious
prototyping, finding the right buffer ICs, etc. Right now, I don't have
time to try this, but *probably* someday in the not so near future.

### What Does it Look Like

Here is a picture of the front

<a href="https://raw.githubusercontent.com/tkurbad/A1200_Kickstart_Relocator/master/image/a1200_kickstart_relocator_pcb_front.jpg"
target="_blank">
<img src="https://raw.githubusercontent.com/tkurbad/A1200_Kickstart_Relocator/master/image/a1200_kickstart_relocator_pcb_front.jpg" 
alt="PCB Rev 1.0 Front" width="400" hspace="10" vspace="10" /></a>

and the back of the revision 1.0 PCB

<a href="https://raw.githubusercontent.com/tkurbad/A1200_Kickstart_Relocator/master/image/a1200_kickstart_relocator_pcb_front.jpg"
target="_blank">
<img src="https://raw.githubusercontent.com/tkurbad/A1200_Kickstart_Relocator/master/image/a1200_kickstart_relocator_pcb_back.jpg" 
alt="PCB Rev 1.0 Back" width="400" hspace="10" vspace="10" /></a>

Current revision is 1.1. For changes, see revision history below

### BOM

This is an easy solder. Here is the table of parts you will need:

|Designator|Part|Total Number |Remark|
|----------|----|------------:|------|
|U6A1, U6B1|DIL Precision Male Header 21 Pin|4||
|U6A_Reloc1, U6B_Reloc1|DIL IC-Socket 42 Pin|2||
|CP Expander|DIL Male Header 2 Pin|1|optional|
|KS Sw. enable, KS Switch|DIL Male Header 3 Pin|2|optional|

The *CP Expander* header only needs to be populated if you want to use a
Clockport Expander.

The *Kickstart Sw. enable* and *KS Switch* headers only need to be
populated if you plan on using the Kickstart switch functionality.
**Note, that you should put a jumper wire between the middle pin and the
pin marked <tt>0</tt> on of the <i>Kickstart Sw. enable</i> header if you don't
populate it.**

### How to Build and Use

To connect the board to the Amiga, you basically have two options
* **Solder it directly** to the place for the Kickstart sockets with "normal"
pin headers. This is **not recommended** for your trusted old Amiga 1200,
since you have to desolder the Kickstart sockets first which can lead to
severe damage to your mainboard if not done properly! It is, however,
recommended if you are building Chucky's ReAmiga, since you can attach
the board right during the build. Be aware, however, that this makes
soldering the MC68EC020 a bit harder.
* Connect it **to the existing Kickstart sockets**. I recommend using
precision DIL pin rows [like these](https://cdn-reichelt.de/bilder/web/artikel_ws/C110/10120536.jpg)
for this purpose. Also, you have to make sure the board is **firmly
seated** in the Kickstart sockets, and perhaps have to fasten it with
some cable ties or the like. *I never tried this, so you have to test for
yourself if it works reliably this way.*

The following picture shows the Revision 1.0 board soldered directly onto
the ReAmiga 1200 PCB and surrounded by a Indivision AGA MK2cr and a
IDE'fix Express (Please excuse the not yet cleaned solder flux around the CPU)

<a href="https://raw.githubusercontent.com/tkurbad/A1200_Kickstart_Relocator/master/image/a1200_kickstart_relocator_in_action.jpg"
target="_blank">
<img src="https://raw.githubusercontent.com/tkurbad/A1200_Kickstart_Relocator/master/image/a1200_kickstart_relocator_in_action.jpg" 
alt="Kickstart Relocator soldered to ReAmiga" width="400" hspace="10" vspace="10" /></a>

If you own a Clockport Expander, you can connect the signals <tt>A</tt>
and <tt>B</tt> to the respective headers.

If you put a jumper on the middle and <tt>1</tt> pins of the *KS Sw. enable*
header, you will be able to switch between a lower and and upper Kickstart
image burned consecutively to a MC27C800 EPROM or some other 1 MB memory IC
using the *KS Switch* header. In the picture above you can see two
[F2R16 Flash ROMs](https://gglabs.us/node/2026) made by [GGLabs](https://gglabs.us)
and refitted with 29F800 1MB Flash ROMs. In the example picture they
hold a stock 3.1.4 Kickstart as well as Chucky's [DiagROM](http://diagrom.com).

**Note** Board Revision 1.0 has a flaw where I confounded the **middle**
and **leftmost** pin of the *KS Sw. enable* header. Thus, to disable the
Kickstart switch function, you bridge the left and middle pins as usual,
but **to enable Kickstart switching, you have to put a small jumper wire
between the leftmost and the rightmost pins of <i>KS Sw. enable</i>.**
This is fixed in Revision 1.1, where you put the jumper on the left to
disable and on the right to enable Kickstart switching.

### Where to Buy

You can order the boards in batches of 3 from <a href="https://aisler.net/p/MZHJVRSS" target="_blank">
<img src="https://cdn2.aisler.net/assets/logo_medium-cb9505c1296f3ff7ab1d6b85cb831e0ff51d10c188a9584528d1b11ac1a5e657.png"
width="90" hspace="10" vspace="10" valign="middle"></a>
or make Gerbers out of the KiCad files and order boards from your
favourite manufacturer.

**If you want to sell boards yourself, contact me first, please!**

### Revision History

#### Rev 1.1

* Make some traces a bit wider
* Correct Kickstart Switch Enable header mistake
* Marked the board as **not** being a FastATA relocator

#### Rev 1.0

* Initial Version
* There's been a mishap with the Kickstart Switch Enable header
* Still marked as FastATA relocator
* Hasn't been publicly released, but three boards were produced

### License

This work is licensed under the terms of 
[Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)](https://creativecommons.org/licenses/by-sa/4.0/)

Author: **Torsten Kurbad**, 2019

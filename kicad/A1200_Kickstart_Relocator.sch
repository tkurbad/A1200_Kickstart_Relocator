EESchema Schematic File Version 4
LIBS:A1200_Kickstart_Relocator-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "A1200 [FastATA and] Kickstart Relocator"
Date "2019-07-26"
Rev "1.1"
Comp "© Torsten Kurbad 2019"
Comment1 "License: Creative Commons Attribution-ShareAlike 4.0 International"
Comment2 "amiga[at]tk-webart.de"
Comment3 "FastATA relocation does NOT work!"
Comment4 ""
$EndDescr
$Comp
L A1200_Kickstart_Relocator:M27C800 U6B_Reloc1
U 1 1 5D37BB25
P 5650 4300
F 0 "U6B_Reloc1" V 5741 3222 50  0000 R CNN
F 1 "M27C800" V 5650 3222 50  0000 R CNN
F 2 "Housings_DIP:DIP-42_W15.24mm_Socket" V 5650 4300 50  0001 C CNN
F 3 "M27C800_Datasheet.pdf" V 5559 3222 50  0000 R CNN
	1    5650 4300
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0104
U 1 1 5D38457F
P 7350 3050
F 0 "#PWR0104" H 7350 2800 50  0001 C CNN
F 1 "GND" H 7400 2900 50  0000 C CNN
F 2 "" H 7350 3050 50  0001 C CNN
F 3 "" H 7350 3050 50  0001 C CNN
	1    7350 3050
	1    0    0    -1  
$EndComp
Entry Wire Line
	6100 1950 6200 2050
Entry Wire Line
	6200 1950 6300 2050
Entry Wire Line
	6300 1950 6400 2050
Entry Wire Line
	6400 1950 6500 2050
Entry Wire Line
	6500 1950 6600 2050
Entry Wire Line
	6600 1950 6700 2050
Entry Wire Line
	6700 1950 6800 2050
Entry Wire Line
	6800 1950 6900 2050
Entry Wire Line
	6900 1950 7000 2050
Entry Wire Line
	7000 1950 7100 2050
Entry Wire Line
	6400 2950 6500 3050
Entry Wire Line
	6500 2950 6600 3050
Entry Wire Line
	6600 2950 6700 3050
Entry Wire Line
	6700 2950 6800 3050
Entry Wire Line
	6800 2950 6900 3050
Entry Wire Line
	6900 2950 7000 3050
Entry Wire Line
	7000 2950 7100 3050
Entry Wire Line
	7100 2950 7200 3050
Text GLabel 6000 1950 0    50   BiDi ~ 0
A[8..16,19]
Text GLabel 6000 3050 0    50   BiDi ~ 0
A[0..7,17]
$Comp
L power:VCC #PWR0116
U 1 1 5D3C8926
P 7150 2050
F 0 "#PWR0116" H 7150 1900 50  0001 C CNN
F 1 "VCC" H 7100 2150 50  0000 C CNN
F 2 "" H 7150 2050 50  0001 C CNN
F 3 "" H 7150 2050 50  0001 C CNN
	1    7150 2050
	1    0    0    -1  
$EndComp
Entry Wire Line
	7400 650  7500 750 
Entry Wire Line
	7500 650  7600 750 
Entry Wire Line
	7600 650  7700 750 
Entry Wire Line
	7700 650  7800 750 
Entry Wire Line
	7800 650  7900 750 
Entry Wire Line
	7900 650  8000 750 
Entry Wire Line
	8000 650  8100 750 
Entry Wire Line
	7500 1650 7600 1750
Entry Wire Line
	7600 1650 7700 1750
Entry Wire Line
	7700 1650 7800 1750
Entry Wire Line
	7800 1650 7900 1750
Entry Wire Line
	7900 1650 8000 1750
Entry Wire Line
	8000 1650 8100 1750
Entry Wire Line
	8100 1650 8200 1750
Entry Wire Line
	8200 1650 8300 1750
Text GLabel 9550 650  2    50   BiDi ~ 0
D[0..15]
Entry Wire Line
	7300 1950 7400 2050
Entry Wire Line
	7400 1950 7500 2050
Entry Wire Line
	7500 1950 7600 2050
Entry Wire Line
	7600 1950 7700 2050
Entry Wire Line
	7700 1950 7800 2050
Entry Wire Line
	7800 1950 7900 2050
Entry Wire Line
	7900 1950 8000 2050
Entry Wire Line
	8000 1950 8100 2050
Entry Wire Line
	7500 2950 7600 3050
Entry Wire Line
	7600 2950 7700 3050
Entry Wire Line
	7700 2950 7800 3050
Entry Wire Line
	7800 2950 7900 3050
Entry Wire Line
	7900 2950 8000 3050
Entry Wire Line
	8000 2950 8100 3050
Entry Wire Line
	8100 2950 8200 3050
Entry Wire Line
	8200 2950 8300 3050
Text GLabel 9550 1950 2    50   BiDi ~ 0
D[16..31]
Wire Wire Line
	7400 3000 7500 3000
Wire Wire Line
	7200 2900 7250 2900
Wire Wire Line
	7250 2900 7250 3150
Wire Wire Line
	7250 3150 7150 3150
Text GLabel 7600 3200 2    50   Input ~ 0
_OE
Text GLabel 7150 3150 0    50   Input ~ 0
_CS
Wire Wire Line
	5650 4650 5700 4650
Wire Wire Line
	7500 3200 7600 3200
Wire Wire Line
	7100 2900 7100 2950
Wire Wire Line
	7000 2900 7000 2950
Wire Wire Line
	6900 2900 6900 2950
Wire Wire Line
	6800 2900 6800 2950
Wire Wire Line
	6700 2900 6700 2950
Wire Wire Line
	6600 2900 6600 2950
Wire Wire Line
	6500 2900 6500 2950
Wire Wire Line
	7300 2900 7350 2900
Wire Wire Line
	7350 2900 7350 3050
Wire Wire Line
	6400 2900 6400 2950
Wire Wire Line
	7400 3000 7400 2900
Wire Wire Line
	8200 2900 8200 2950
Wire Wire Line
	8100 2900 8100 2950
Wire Wire Line
	8000 2900 8000 2950
Wire Wire Line
	7900 2900 7900 2950
Wire Wire Line
	7800 2900 7800 2950
Wire Wire Line
	7700 2900 7700 2950
Wire Wire Line
	7600 2900 7600 2950
Wire Wire Line
	7500 2900 7500 2950
Wire Wire Line
	7500 1600 7500 1650
Wire Wire Line
	7600 1600 7600 1650
Wire Wire Line
	7700 1600 7700 1650
Wire Wire Line
	7800 1600 7800 1650
Wire Wire Line
	7900 1600 7900 1650
Wire Wire Line
	8000 1600 8000 1650
Wire Wire Line
	8100 1600 8100 1650
Wire Wire Line
	8200 1600 8200 1650
Wire Wire Line
	7500 3000 7500 3200
$Comp
L A1200_Kickstart_Relocator:M27C800 U6A1
U 1 1 5D37398D
P 7200 2550
F 0 "U6A1" V 7291 1472 50  0000 R CNN
F 1 "M27C800" V 7200 1472 50  0000 R CNN
F 2 "Housings_DIP:DIP-42_W15.24mm_Socket" V 7200 2550 50  0001 C CNN
F 3 "M27C800_Datasheet.pdf" V 7109 1472 50  0000 R CNN
	1    7200 2550
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6200 2050 6200 2200
Wire Wire Line
	6300 2050 6300 2200
Wire Wire Line
	6400 2050 6400 2200
Wire Wire Line
	6500 2050 6500 2200
Wire Wire Line
	6600 2050 6600 2200
Wire Wire Line
	6700 2050 6700 2200
Wire Wire Line
	6800 2050 6800 2200
Wire Wire Line
	6900 2050 6900 2200
Wire Wire Line
	7000 2050 7000 2200
Wire Wire Line
	7100 2050 7100 2200
Text Label 6200 2200 1    35   ~ 0
A19
Text Label 6300 2200 1    35   ~ 0
A8
Text Label 6400 2200 1    35   ~ 0
A9
Text Label 6500 2200 1    35   ~ 0
A10
Text Label 6600 2200 1    35   ~ 0
A11
Text Label 6700 2200 1    35   ~ 0
A12
Text Label 6800 2200 1    35   ~ 0
A13
Text Label 6900 2200 1    35   ~ 0
A14
Text Label 7000 2200 1    35   ~ 0
A15
Text Label 7100 2200 1    35   ~ 0
A16
Text Label 6400 2950 1    35   ~ 0
A7
Text Label 6500 2950 1    35   ~ 0
A6
Text Label 6600 2950 1    35   ~ 0
A5
Text Label 6700 2950 1    35   ~ 0
A4
Text Label 6800 2950 1    35   ~ 0
A3
Text Label 6900 2950 1    35   ~ 0
A2
Text Label 7000 2950 1    35   ~ 0
A1
Text Label 7100 2950 1    35   ~ 0
A0
Text Label 7200 2200 1    35   ~ 0
VCC
Wire Bus Line
	9350 1750 9350 650 
Connection ~ 9350 650 
Wire Bus Line
	9350 650  9550 650 
Wire Wire Line
	7400 2050 7400 2200
Wire Wire Line
	7500 2050 7500 2200
Wire Wire Line
	7600 2050 7600 2200
Wire Wire Line
	7700 2050 7700 2200
Wire Wire Line
	7800 2050 7800 2200
Wire Wire Line
	7200 2050 7200 2200
Wire Wire Line
	7150 2050 7200 2050
Wire Wire Line
	7900 2050 7900 2200
Wire Wire Line
	8000 2050 8000 2200
Wire Wire Line
	8100 2050 8100 2200
Wire Wire Line
	8100 750  8100 900 
Wire Wire Line
	8000 750  8000 900 
Wire Wire Line
	7900 750  7900 900 
Wire Wire Line
	7800 750  7800 900 
Wire Wire Line
	7700 750  7700 900 
Wire Wire Line
	7600 750  7600 900 
Wire Wire Line
	7500 750  7500 900 
Wire Wire Line
	7400 750  7400 900 
Text Label 7250 3050 1    35   ~ 0
_CS
Text Label 7350 3050 1    35   ~ 0
GND
Text Label 7500 3150 1    35   ~ 0
_OE
Wire Bus Line
	9350 3050 9350 1950
Connection ~ 9350 1950
Wire Bus Line
	9350 1950 9550 1950
Text Label 7400 900  1    35   ~ 0
D15
Text Label 7500 900  1    35   ~ 0
D7
Text Label 7600 900  1    35   ~ 0
D14
Text Label 7700 900  1    35   ~ 0
D6
Text Label 7900 900  1    35   ~ 0
D5
Text Label 8100 900  1    35   ~ 0
D4
Text Label 7800 900  1    35   ~ 0
D13
Text Label 8000 900  1    35   ~ 0
D12
Text Label 8200 1600 3    35   ~ 0
D11
Text Label 8000 1600 3    35   ~ 0
D10
Text Label 8100 1600 3    35   ~ 0
D3
Text Label 7900 1600 3    35   ~ 0
D2
Text Label 7800 1600 3    35   ~ 0
D9
Text Label 7700 1600 3    35   ~ 0
D1
Text Label 7600 1600 3    35   ~ 0
D8
Text Label 7500 1600 3    35   ~ 0
D0
Text Label 7500 2900 3    35   ~ 0
D16
Text Label 7700 2900 3    35   ~ 0
D17
Text Label 7900 2900 3    35   ~ 0
D18
Text Label 8100 2900 3    35   ~ 0
D19
Text Label 8100 2200 1    35   ~ 0
D20
Text Label 7900 2200 1    35   ~ 0
D21
Text Label 7700 2200 1    35   ~ 0
D22
Text Label 7500 2200 1    35   ~ 0
D23
Text Label 7600 2900 3    35   ~ 0
D24
Text Label 7800 2900 3    35   ~ 0
D25
Text Label 8000 2900 3    35   ~ 0
D26
Text Label 8200 2900 3    35   ~ 0
D27
Text Label 8000 2200 1    35   ~ 0
D28
Text Label 7800 2200 1    35   ~ 0
D29
Text Label 7600 2200 1    35   ~ 0
D30
Text Label 7400 2200 1    35   ~ 0
D31
$Comp
L power:GND #PWR0101
U 1 1 5E045131
P 5700 3800
F 0 "#PWR0101" H 5700 3550 50  0001 C CNN
F 1 "GND" H 5650 3650 50  0000 C CNN
F 2 "" H 5700 3800 50  0001 C CNN
F 3 "" H 5700 3800 50  0001 C CNN
	1    5700 3800
	-1   0    0    1   
$EndComp
Entry Wire Line
	4550 3700 4650 3800
Entry Wire Line
	4750 3700 4850 3800
Entry Wire Line
	4850 3700 4950 3800
Entry Wire Line
	4950 3700 5050 3800
Entry Wire Line
	5050 3700 5150 3800
Entry Wire Line
	5150 3700 5250 3800
Entry Wire Line
	5250 3700 5350 3800
Entry Wire Line
	5350 3700 5450 3800
Entry Wire Line
	5450 3700 5550 3800
Text GLabel 4450 3700 0    50   BiDi ~ 0
A[8..16,19]
$Comp
L power:VCC #PWR0102
U 1 1 5E045145
P 5600 3750
F 0 "#PWR0102" H 5600 3600 50  0001 C CNN
F 1 "VCC" H 5550 3900 50  0000 C CNN
F 2 "" H 5600 3750 50  0001 C CNN
F 3 "" H 5600 3750 50  0001 C CNN
	1    5600 3750
	1    0    0    -1  
$EndComp
Entry Wire Line
	5850 3700 5950 3800
Entry Wire Line
	5950 3700 6050 3800
Entry Wire Line
	6050 3700 6150 3800
Entry Wire Line
	6150 3700 6250 3800
Entry Wire Line
	6250 3700 6350 3800
Entry Wire Line
	6350 3700 6450 3800
Entry Wire Line
	6450 3700 6550 3800
Text GLabel 8000 3700 2    50   BiDi ~ 0
D[0..15]
Entry Wire Line
	4650 3700 4750 3800
Wire Wire Line
	4650 3800 4650 3950
Wire Wire Line
	4750 3800 4750 3950
Wire Wire Line
	4850 3800 4850 3950
Wire Wire Line
	4950 3800 4950 3950
Wire Wire Line
	5050 3800 5050 3950
Wire Wire Line
	5150 3800 5150 3950
Wire Wire Line
	5250 3800 5250 3950
Wire Wire Line
	5350 3800 5350 3950
Wire Wire Line
	5450 3800 5450 3950
Wire Wire Line
	5550 3800 5550 3950
Text Label 4650 3950 1    35   ~ 0
A19
Text Label 4950 3950 1    35   ~ 0
A10
Text Label 5050 3950 1    35   ~ 0
A11
Text Label 5150 3950 1    35   ~ 0
A12
Text Label 5250 3950 1    35   ~ 0
A13
Text Label 5350 3950 1    35   ~ 0
A14
Text Label 5450 3950 1    35   ~ 0
A15
Text Label 5550 3950 1    35   ~ 0
A16
Wire Wire Line
	5600 3750 5600 3800
Wire Wire Line
	5600 3800 5650 3800
Wire Wire Line
	5650 3800 5650 3950
Text Label 5650 3950 1    35   ~ 0
VCC
Entry Wire Line
	5750 3700 5850 3800
Wire Wire Line
	5700 3800 5750 3800
Wire Wire Line
	5750 3800 5750 3950
Text Label 5750 3950 1    35   ~ 0
GND
Wire Wire Line
	6550 3800 6550 3950
Wire Wire Line
	6450 3800 6450 3950
Wire Wire Line
	6350 3800 6350 3950
Wire Wire Line
	6250 3800 6250 3950
Wire Wire Line
	6150 3800 6150 3950
Wire Wire Line
	6050 3800 6050 3950
Wire Wire Line
	5950 3800 5950 3950
Wire Wire Line
	5850 3800 5850 3950
Text Label 5850 3950 1    35   ~ 0
D15
Text Label 6050 3950 1    35   ~ 0
D14
Text Label 6250 3950 1    35   ~ 0
D13
Text Label 6450 3950 1    35   ~ 0
D12
Text Label 4750 3950 1    35   ~ 0
A8
Text Label 4850 3950 1    35   ~ 0
A9
Text Label 5950 3950 1    35   ~ 0
D7
Text Label 6150 3950 1    35   ~ 0
D6
Text Label 6350 3950 1    35   ~ 0
D5
Text Label 6550 3950 1    35   ~ 0
D4
$Comp
L power:VCC #PWR0103
U 1 1 5E0BA0A8
P 6800 3950
F 0 "#PWR0103" H 6800 3800 50  0001 C CNN
F 1 "VCC" H 6900 4050 50  0000 C CNN
F 2 "" H 6800 3950 50  0001 C CNN
F 3 "" H 6800 3950 50  0001 C CNN
	1    6800 3950
	0    1    1    0   
$EndComp
Wire Wire Line
	6650 3950 6800 3950
Text Label 6800 3950 2    35   ~ 0
VCC
$Comp
L A1200_Kickstart_Relocator:M27C800 U6A_Reloc1
U 1 1 5D37DF18
P 5650 5600
F 0 "U6A_Reloc1" V 5741 4522 50  0000 R CNN
F 1 "M27C800" V 5650 4522 50  0000 R CNN
F 2 "Housings_DIP:DIP-42_W15.24mm_Socket" V 5650 5600 50  0001 C CNN
F 3 "M27C800_Datasheet.pdf" V 5559 4522 50  0000 R CNN
	1    5650 5600
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0108
U 1 1 5E153002
P 5800 4750
F 0 "#PWR0108" H 5800 4500 50  0001 C CNN
F 1 "GND" H 5800 4600 50  0000 C CNN
F 2 "" H 5800 4750 50  0001 C CNN
F 3 "" H 5800 4750 50  0001 C CNN
	1    5800 4750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0109
U 1 1 5E15300C
P 5700 5100
F 0 "#PWR0109" H 5700 4850 50  0001 C CNN
F 1 "GND" H 5700 4950 50  0000 C CNN
F 2 "" H 5700 5100 50  0001 C CNN
F 3 "" H 5700 5100 50  0001 C CNN
	1    5700 5100
	-1   0    0    1   
$EndComp
Entry Wire Line
	4550 5000 4650 5100
Entry Wire Line
	4650 5000 4750 5100
Entry Wire Line
	4750 5000 4850 5100
Entry Wire Line
	4850 5000 4950 5100
Entry Wire Line
	4950 5000 5050 5100
Entry Wire Line
	5050 5000 5150 5100
Entry Wire Line
	5150 5000 5250 5100
Entry Wire Line
	5250 5000 5350 5100
Entry Wire Line
	5350 5000 5450 5100
Entry Wire Line
	5450 5000 5550 5100
Entry Wire Line
	4750 4700 4850 4800
Entry Wire Line
	4850 4700 4950 4800
Entry Wire Line
	4950 4700 5050 4800
Entry Wire Line
	5050 4700 5150 4800
Entry Wire Line
	5150 4700 5250 4800
Entry Wire Line
	5250 4700 5350 4800
Entry Wire Line
	5350 4700 5450 4800
Entry Wire Line
	5450 4700 5550 4800
Entry Wire Line
	5550 4700 5650 4800
Text GLabel 4450 5000 0    50   BiDi ~ 0
A[8..16,19]
Text GLabel 4450 4800 0    50   BiDi ~ 0
A[0..7,17]
$Comp
L power:VCC #PWR0110
U 1 1 5E15302C
P 5600 5100
F 0 "#PWR0110" H 5600 4950 50  0001 C CNN
F 1 "VCC" H 5550 5200 50  0000 C CNN
F 2 "" H 5600 5100 50  0001 C CNN
F 3 "" H 5600 5100 50  0001 C CNN
	1    5600 5100
	1    0    0    -1  
$EndComp
Entry Wire Line
	5950 4700 6050 4800
Entry Wire Line
	6050 4700 6150 4800
Entry Wire Line
	6150 4700 6250 4800
Entry Wire Line
	6250 4700 6350 4800
Entry Wire Line
	6350 4700 6450 4800
Entry Wire Line
	6450 4700 6550 4800
Entry Wire Line
	6550 4700 6650 4800
Entry Wire Line
	6650 4700 6750 4800
Entry Wire Line
	5750 5000 5850 5100
Entry Wire Line
	5850 5000 5950 5100
Entry Wire Line
	5950 5000 6050 5100
Entry Wire Line
	6050 5000 6150 5100
Entry Wire Line
	6150 5000 6250 5100
Entry Wire Line
	6250 5000 6350 5100
Entry Wire Line
	6350 5000 6450 5100
Entry Wire Line
	6450 5000 6550 5100
Text GLabel 8000 5000 2    50   BiDi ~ 0
D[16..31]
Wire Wire Line
	5850 4650 5850 4750
Wire Wire Line
	5850 4750 5950 4750
Wire Wire Line
	5950 4750 5950 4900
Text GLabel 6050 4900 2    50   Input ~ 0
_OE
Text GLabel 5600 4900 0    50   Input ~ 0
_CS
Wire Wire Line
	5700 4650 5700 4900
Wire Wire Line
	5700 4900 5600 4900
Wire Wire Line
	5800 4650 5800 4750
Wire Wire Line
	4650 4650 4650 4700
Wire Wire Line
	4750 4650 4750 4700
Wire Wire Line
	4850 4650 4850 4700
Wire Wire Line
	4950 4650 4950 4700
Wire Wire Line
	5050 4650 5050 4700
Wire Wire Line
	5150 4650 5150 4700
Wire Wire Line
	5250 4650 5250 4700
Wire Wire Line
	5350 4650 5350 4700
Wire Wire Line
	5450 4650 5450 4700
Wire Wire Line
	5550 4650 5550 4700
Wire Wire Line
	5950 4650 5950 4700
Wire Wire Line
	6050 4650 6050 4700
Wire Wire Line
	6150 4650 6150 4700
Wire Wire Line
	6250 4650 6250 4700
Wire Wire Line
	6350 4650 6350 4700
Wire Wire Line
	6450 4650 6450 4700
Wire Wire Line
	6550 4650 6550 4700
Wire Wire Line
	6650 4650 6650 4700
Wire Wire Line
	4650 5100 4650 5250
Wire Wire Line
	4750 5100 4750 5250
Wire Wire Line
	4850 5100 4850 5250
Wire Wire Line
	4950 5100 4950 5250
Wire Wire Line
	5050 5100 5050 5250
Wire Wire Line
	5250 5100 5250 5250
Wire Wire Line
	5350 5100 5350 5250
Wire Wire Line
	5450 5100 5450 5250
Wire Wire Line
	5550 5100 5550 5250
Text Label 4650 5250 1    35   ~ 0
A19
Text Label 4750 5250 1    35   ~ 0
A8
Text Label 4850 5250 1    35   ~ 0
A9
Text Label 5700 4800 1    35   ~ 0
_CS
Text Label 5950 4850 1    35   ~ 0
_OE
Text Label 5800 4750 1    35   ~ 0
GND
Text Label 4650 4700 1    35   ~ 0
A18R
Text Label 4750 4700 1    35   ~ 0
A17
Text Label 4850 4700 1    35   ~ 0
A7
Text Label 4950 4700 1    35   ~ 0
A6
Text Label 5050 4700 1    35   ~ 0
A5
Text Label 5150 4700 1    35   ~ 0
A4
Text Label 5250 4700 1    35   ~ 0
A3
Text Label 5350 4700 1    35   ~ 0
A2
Text Label 5450 4700 1    35   ~ 0
A1
Text Label 5550 4700 1    35   ~ 0
A0
Text Label 4950 5250 1    35   ~ 0
A10
Text Label 5050 5250 1    35   ~ 0
A11
Text Label 5150 5250 1    35   ~ 0
A12
Text Label 5250 5250 1    35   ~ 0
A13
Text Label 5350 5250 1    35   ~ 0
A14
Text Label 5450 5250 1    35   ~ 0
A15
Text Label 5550 5250 1    35   ~ 0
A16
Text Label 5650 5250 1    35   ~ 0
VCC
Text Label 5750 5250 1    35   ~ 0
GND
Wire Bus Line
	7800 4800 7800 3700
Wire Wire Line
	6050 4900 5950 4900
Wire Wire Line
	5850 5100 5850 5250
Wire Wire Line
	5950 5100 5950 5250
Wire Wire Line
	6050 5100 6050 5250
Wire Wire Line
	6150 5100 6150 5250
Wire Wire Line
	6250 5100 6250 5250
Wire Wire Line
	5700 5100 5750 5100
Wire Wire Line
	5750 5100 5750 5250
Wire Wire Line
	5650 5100 5650 5250
Wire Wire Line
	5600 5100 5650 5100
Wire Wire Line
	6350 5100 6350 5250
Wire Wire Line
	6450 5100 6450 5250
Wire Wire Line
	6550 5100 6550 5250
Wire Bus Line
	7800 6100 7800 5000
Connection ~ 7800 5000
Wire Bus Line
	7800 5000 8000 5000
Text Label 6650 4650 3    35   ~ 0
D11
Text Label 6450 4650 3    35   ~ 0
D10
Text Label 6550 4650 3    35   ~ 0
D3
Text Label 6350 4650 3    35   ~ 0
D2
Text Label 6250 4650 3    35   ~ 0
D9
Text Label 6150 4650 3    35   ~ 0
D1
Text Label 6050 4650 3    35   ~ 0
D8
Text Label 5950 4650 3    35   ~ 0
D0
Text Label 6550 5250 1    35   ~ 0
D20
Text Label 6350 5250 1    35   ~ 0
D21
Text Label 6150 5250 1    35   ~ 0
D22
Text Label 5950 5250 1    35   ~ 0
D23
Text Label 6450 5250 1    35   ~ 0
D28
Text Label 6250 5250 1    35   ~ 0
D29
Text Label 6050 5250 1    35   ~ 0
D30
Text Label 5850 5250 1    35   ~ 0
D31
Connection ~ 7800 3700
Wire Bus Line
	7800 3700 8000 3700
$Comp
L power:VCC #PWR0111
U 1 1 5E175DA5
P 6800 5250
F 0 "#PWR0111" H 6800 5100 50  0001 C CNN
F 1 "VCC" H 6900 5350 50  0000 C CNN
F 2 "" H 6800 5250 50  0001 C CNN
F 3 "" H 6800 5250 50  0001 C CNN
	1    6800 5250
	0    1    1    0   
$EndComp
Wire Wire Line
	6650 5250 6800 5250
Text Label 6700 5250 0    35   ~ 0
VCC
$Comp
L power:GND #PWR0112
U 1 1 5E1A4C93
P 5800 6100
F 0 "#PWR0112" H 5800 5850 50  0001 C CNN
F 1 "GND" H 5850 5950 50  0000 C CNN
F 2 "" H 5800 6100 50  0001 C CNN
F 3 "" H 5800 6100 50  0001 C CNN
	1    5800 6100
	1    0    0    -1  
$EndComp
Entry Wire Line
	4750 6000 4850 6100
Entry Wire Line
	4850 6000 4950 6100
Entry Wire Line
	4950 6000 5050 6100
Entry Wire Line
	5050 6000 5150 6100
Entry Wire Line
	5150 6000 5250 6100
Entry Wire Line
	5250 6000 5350 6100
Entry Wire Line
	5350 6000 5450 6100
Entry Wire Line
	5450 6000 5550 6100
Entry Wire Line
	5550 6000 5650 6100
Text GLabel 4450 6100 0    50   BiDi ~ 0
A[0..7,17]
Entry Wire Line
	5950 6000 6050 6100
Entry Wire Line
	6050 6000 6150 6100
Entry Wire Line
	6150 6000 6250 6100
Entry Wire Line
	6250 6000 6350 6100
Entry Wire Line
	6350 6000 6450 6100
Entry Wire Line
	6450 6000 6550 6100
Entry Wire Line
	6550 6000 6650 6100
Entry Wire Line
	6650 6000 6750 6100
Wire Wire Line
	5850 6050 5950 6050
Wire Wire Line
	5700 5950 5700 6200
Wire Wire Line
	5700 6200 5600 6200
Text GLabel 5600 6200 0    50   Input ~ 0
_CS
Wire Wire Line
	5550 5950 5550 6000
Wire Wire Line
	5450 5950 5450 6000
Wire Wire Line
	5350 5950 5350 6000
Wire Wire Line
	5250 5950 5250 6000
Wire Wire Line
	5150 5950 5150 6000
Wire Wire Line
	5050 5950 5050 6000
Wire Wire Line
	4950 5950 4950 6000
Wire Wire Line
	4750 5950 4750 6000
Wire Wire Line
	4650 5950 4650 6000
Wire Wire Line
	5800 5950 5800 6100
Wire Wire Line
	4850 5950 4850 6000
Wire Wire Line
	5850 6050 5850 5950
Wire Wire Line
	6650 5950 6650 6000
Wire Wire Line
	6550 5950 6550 6000
Wire Wire Line
	6450 5950 6450 6000
Wire Wire Line
	6350 5950 6350 6000
Wire Wire Line
	6250 5950 6250 6000
Wire Wire Line
	6150 5950 6150 6000
Wire Wire Line
	6050 5950 6050 6000
Wire Wire Line
	5950 5950 5950 6000
Wire Wire Line
	5950 6050 5950 6250
Text Label 4650 6000 1    35   ~ 0
A18R
Text Label 4750 6000 1    35   ~ 0
A17
Text Label 4850 6000 1    35   ~ 0
A7
Text Label 4950 6000 1    35   ~ 0
A6
Text Label 5050 6000 1    35   ~ 0
A5
Text Label 5150 6000 1    35   ~ 0
A4
Text Label 5250 6000 1    35   ~ 0
A3
Text Label 5350 6000 1    35   ~ 0
A2
Text Label 5450 6000 1    35   ~ 0
A1
Text Label 5550 6000 1    35   ~ 0
A0
Text Label 5700 6100 1    35   ~ 0
_CS
Text Label 5800 6100 1    35   ~ 0
GND
Text Label 5950 6200 1    35   ~ 0
_OE
Text Label 5950 5950 3    35   ~ 0
D16
Text Label 6150 5950 3    35   ~ 0
D17
Text Label 6350 5950 3    35   ~ 0
D18
Text Label 6550 5950 3    35   ~ 0
D19
Text Label 6050 5950 3    35   ~ 0
D24
Text Label 6250 5950 3    35   ~ 0
D25
Text Label 6450 5950 3    35   ~ 0
D26
Text Label 6650 5950 3    35   ~ 0
D27
Text GLabel 6050 6250 2    50   Input ~ 0
_OE
Wire Wire Line
	5950 6250 6050 6250
Wire Wire Line
	5800 5950 5750 5950
Wire Wire Line
	5650 5950 5700 5950
Wire Wire Line
	5750 4650 5800 4650
$Comp
L Connector:Conn_01x03_Male J1
U 1 1 5E2601E9
P 3500 4550
F 0 "J1" H 3608 4831 50  0000 C CNN
F 1 "KS Sw. Enable" H 3608 4740 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x03_Pitch2.54mm" H 3500 4550 50  0001 C CNN
F 3 "~" H 3500 4550 50  0001 C CNN
	1    3500 4550
	0    -1   -1   0   
$EndComp
$Comp
L Connector:Conn_01x03_Male J2
U 1 1 5E298898
P 2900 3700
F 0 "J2" H 3008 3981 50  0000 C CNN
F 1 "KS Switch" H 3008 3890 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x03_Pitch2.54mm" H 2900 3700 50  0001 C CNN
F 3 "~" H 2900 3700 50  0001 C CNN
	1    2900 3700
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Male J3
U 1 1 5E2B315E
P 4050 5400
F 0 "J3" H 4158 5581 50  0000 C CNN
F 1 "CP Exp." H 4158 5490 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 4050 5400 50  0001 C CNN
F 3 "~" H 4050 5400 50  0001 C CNN
	1    4050 5400
	1    0    0    -1  
$EndComp
Text Label 6300 2950 1    35   ~ 0
A17
Text Label 6200 2950 1    35   ~ 0
A18
Wire Wire Line
	6200 2900 6200 2950
Wire Wire Line
	6300 2900 6300 2950
Entry Wire Line
	6300 2950 6400 3050
Wire Wire Line
	4650 6000 3800 6000
Wire Wire Line
	3800 6000 3800 4700
Wire Wire Line
	3800 4700 4650 4700
Wire Wire Line
	3100 3600 3200 3600
Wire Wire Line
	3200 3600 3200 3500
Wire Wire Line
	3100 3800 3200 3800
Wire Wire Line
	3200 3800 3200 3900
$Comp
L power:GND #PWR0117
U 1 1 5E42A75C
P 3200 3500
F 0 "#PWR0117" H 3200 3250 50  0001 C CNN
F 1 "GND" H 3205 3327 50  0000 C CNN
F 2 "" H 3200 3500 50  0001 C CNN
F 3 "" H 3200 3500 50  0001 C CNN
	1    3200 3500
	-1   0    0    1   
$EndComp
$Comp
L power:VCC #PWR0118
U 1 1 5E42B18F
P 3200 3900
F 0 "#PWR0118" H 3200 3750 50  0001 C CNN
F 1 "VCC" H 3218 4073 50  0000 C CNN
F 2 "" H 3200 3900 50  0001 C CNN
F 3 "" H 3200 3900 50  0001 C CNN
	1    3200 3900
	-1   0    0    1   
$EndComp
Connection ~ 3800 4700
Wire Wire Line
	5150 5400 4250 5400
Wire Wire Line
	5150 5100 5150 5250
Connection ~ 5150 5250
Wire Wire Line
	5150 5250 5150 5400
Wire Wire Line
	5250 5250 5250 5500
Wire Wire Line
	5250 5500 4250 5500
Connection ~ 5250 5250
Text Label 4300 5500 2    50   ~ 0
A
Text Label 4300 5400 2    50   ~ 0
B
$Comp
L A1200_Kickstart_Relocator:M27C800 U6B1
U 1 1 5D3749AF
P 7200 1250
F 0 "U6B1" V 7291 172 50  0000 R CNN
F 1 "M27C800" V 7200 172 50  0000 R CNN
F 2 "Housings_DIP:DIP-42_W15.24mm_Socket" V 7200 1250 50  0001 C CNN
F 3 "M27C800_Datasheet.pdf" V 7109 172 50  0000 R CNN
	1    7200 1250
	0    -1   -1   0   
$EndComp
Entry Wire Line
	7300 650  7400 750 
Wire Wire Line
	3600 2950 6200 2950
Wire Wire Line
	3600 2950 3600 4350
Wire Wire Line
	3400 4350 3400 3700
Wire Wire Line
	3400 3700 3100 3700
Wire Wire Line
	3800 4200 3500 4200
Wire Wire Line
	3500 4200 3500 4350
Wire Wire Line
	3800 4200 3800 4700
Wire Bus Line
	4450 3700 5450 3700
Wire Bus Line
	6050 6100 7800 6100
Wire Bus Line
	4450 6100 5650 6100
Wire Bus Line
	5750 5000 7800 5000
Wire Bus Line
	6050 4800 7800 4800
Wire Bus Line
	4450 4800 5650 4800
Wire Bus Line
	4450 5000 5450 5000
Wire Bus Line
	5750 3700 7800 3700
Wire Bus Line
	7600 3050 9350 3050
Wire Bus Line
	7300 1950 9350 1950
Wire Bus Line
	7600 1750 9350 1750
Wire Bus Line
	7300 650  9350 650 
Wire Bus Line
	6000 3050 7200 3050
Wire Bus Line
	6000 1950 7000 1950
$EndSCHEMATC
